import {
  SET_TODO,
  GET_TODO,
  SET_TODO_FILTER,
  SET_TODO_REMOVE,
  UPDATE_TODO_STATUS,
  SET_TODO_CLEAR_STATUS_DONE,
  UPDATE_TODO_DATA,
} from '@containers/Todo/constants';

export const setTodoClearStatus = (todoStatus) => ({
  type: SET_TODO_CLEAR_STATUS_DONE,
  todoStatus,
});

export const updateTodoStatus = (todoUpdate) => ({
  type: UPDATE_TODO_STATUS,
  todoUpdate,
});

export const updateTodoData = (todoData) => ({
  type: UPDATE_TODO_DATA,
  todoData,
});

export const setTodoRemove = (todoRemove) => ({
  type: SET_TODO_REMOVE,
  todoRemove,
});

export const setTodoFilter = (todoFilter) => ({
  type: SET_TODO_FILTER,
  todoFilter,
});

export const setToDos = (todos) => ({
  type: SET_TODO,
  todos,
});

export const getCountryList = () => ({
  type: GET_TODO,
});
