import { createSelector } from 'reselect';
import { initialState } from '@containers/Todo/reducer';

const selectTodoState = (state) => state.todo || initialState;

const selectTodoList = createSelector(selectTodoState, (state) => state.todos);
const selectTodoFilter = createSelector(selectTodoState, (state) => state.todoFilter);

export { selectTodoList, selectTodoFilter };
