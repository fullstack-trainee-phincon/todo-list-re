import { produce } from 'immer';

import {
  SET_TODO,
  SET_TODO_FILTER,
  SET_TODO_CLEAR_STATUS_DONE,
  SET_TODO_REMOVE,
  UPDATE_TODO_STATUS,
  UPDATE_TODO_DATA,
} from '@containers/Todo/constants';

export const storedKeyTodo = [''];

export const initialState = {
  todos: [],
  todoFilter: 'all',
};
let todoIndex;
// eslint-disable-next-line default-param-last
const todoReducer = (state = initialState, action) =>
  produce(state, (draft) => {
    switch (action.type) {
      case SET_TODO:
        draft.todos.push(action.todos);
        break;
      case SET_TODO_FILTER:
        draft.todoFilter = action.todoFilter;
        break;
      case SET_TODO_REMOVE:
        draft.todos = draft.todos.filter((todo) => todo.id !== action.todoRemove);
        break;
      case UPDATE_TODO_STATUS:
        todoIndex = draft.todos.findIndex((todo) => todo.id === action.todoUpdate.id);
        if (todoIndex !== -1) {
          draft.todos[todoIndex].status = action.todoUpdate.status;
        }
        break;
      case UPDATE_TODO_DATA:
        todoIndex = draft.todos.findIndex((todo) => todo.id === action.todoData.id);
        if (todoIndex !== -1) {
          draft.todos[todoIndex].data = action.todoData.data;
        }
        break;
      case SET_TODO_CLEAR_STATUS_DONE:
        draft.todos = draft.todos.filter((todo) => todo.status !== action.todoStatus);
        break;
      default:
        break;
    }
  });

export default todoReducer;
