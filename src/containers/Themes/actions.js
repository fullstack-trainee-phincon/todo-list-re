import { SET_THEME } from '@containers/Themes/constants';

export const setTheme = (theme) => ({
  type: SET_THEME,
  theme,
});
