import React, { useState } from 'react';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Modal from '@mui/material/Modal';
import TextField from '@mui/material/TextField';
import { updateTodoData } from '@containers/Todo/actions';

import { useDispatch } from 'react-redux';

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: 'none',
  boxShadow: 24,
  borderRadius: '8px',
  p: 4,
  fontSize: '10px',
};

export default function BasicModal({ isOpen, handleToClose, todoToEdit, handleTodoUpdate, setTodoToEdit }) {
  const dispatch = useDispatch();
  const [updatedData, setUpdatedData] = useState(todoToEdit?.data || '');

  const handleSaveChanges = () => {
    handleTodoUpdate({ ...todoToEdit, data: updatedData });
    dispatch(updateTodoData({ id: todoToEdit.id, data: updatedData }));
    handleToClose();
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      handleSaveChanges();
    }
  };

  return (
    <div>
      <Modal
        open={isOpen}
        onClose={handleToClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            Edit your Activity
          </Typography>
          {todoToEdit && (
            <div>
              <TextField
                id="outlined-basic"
                label="Activity"
                variant="outlined"
                value={updatedData}
                onChange={(e) => setUpdatedData(e.target.value)}
                onKeyDown={handleKeyPress}
              />
            </div>
          )}
          <Button onClick={handleSaveChanges}>Save Changes</Button>
        </Box>
      </Modal>
    </div>
  );
}
