import TodoLayout from '@layouts/TodoLayout/index';
import TodoHome from '@pages/TodoHome/TodoHome';
import NotFound from '@pages/NotFound';

const routes = [
  {
    path: '/',
    name: 'TodoHome',
    component: TodoHome,
    layout: TodoLayout,
  },
  {
    path: '*',
    name: 'Not Found',
    component: NotFound,
  },
];

export default routes;
