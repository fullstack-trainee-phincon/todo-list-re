import React from 'react';
import PropTypes from 'prop-types';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { selectTheme } from '@containers/Themes/selectors';
import { createStructuredSelector } from 'reselect';
import { connect } from 'react-redux';
import classes from './style.module.scss';

const TodoLayout = ({ theme: myTheme, children }) => {
  const isLight = myTheme === 'dark';

  console.log(myTheme);
  const theme = createTheme({
    palette: {
      background: {
        default: isLight ? 'hsl(207, 26%, 17%)' : 'hsl(0, 0%, 98%)',
        paper: isLight ? 'hsl(237, 14%, 26%)' : 'hsl(0, 0%, 98%)',
      },
      text: {
        primary: isLight ? 'hsl(241, 19%, 90%)' : 'hsl(200, 15%, 8%)',
      },
    },
  });

  return (
    <ThemeProvider theme={theme}>
      <div className={isLight ? classes.containerDark : classes.container}>
        <div className={isLight ? classes.backgroundDark : classes.background}>{children}</div>
      </div>
    </ThemeProvider>
  );
};

TodoLayout.propTypes = {
  theme: PropTypes.string,
  // children: PropTypes.node,
};

const mapStateToProps = createStructuredSelector({
  theme: selectTheme,
});

export default connect(mapStateToProps)(TodoLayout);
