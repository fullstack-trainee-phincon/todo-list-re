import React, { useState, useRef, useEffect } from 'react';
import classes from './style.module.scss';

import { setTheme } from '@containers/Themes/actions';
import { selectTheme } from '@containers/Themes/selectors';

import { connect, useDispatch, useSelector } from 'react-redux';

import PropTypes from 'prop-types';
import { createStructuredSelector } from 'reselect';

import uuid from 'react-uuid';

import Stack from '@mui/material/Stack';
import NightsStayIcon from '@mui/icons-material/NightsStay';
import WbSunnyIcon from '@mui/icons-material/WbSunny';
import IconButton from '@mui/material/IconButton';

import Paper from '@mui/material/Paper';
import InputBase from '@mui/material/InputBase';
import RadioButtonUncheckedIcon from '@mui/icons-material/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@mui/icons-material/RadioButtonChecked';
import Checkbox from '@mui/material/Checkbox';
import CloseIcon from '@mui/icons-material/Close';
import EditIcon from '@mui/icons-material/Edit';

import BasicModal from '@components/modal/modal';
import Grid from '@mui/material/Grid';

import {
  setToDos,
  setTodoClearStatus,
  setTodoFilter,
  setTodoRemove,
  updateTodoStatus,
  updateTodoData,
} from '@containers/Todo/actions';
import { selectTodoFilter, selectTodoList } from '@containers/Todo/selectors';

const TodoHome = ({ todos, todoFilter, theme }) => {
  // ---modal---
  const [openModal, setOpenModal] = useState(false);
  const [todoToEdit, setTodoToEdit] = useState(null);
  const [updatedData, setUpdatedData] = useState('');

  const handleOpen = (todo) => {
    setTodoToEdit(todo);
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  };

  // ---modal---

  const STATUS_DONE = '';
  const STATUS_NOT_DONE = '';
  const dispatch = useDispatch();
  const inputRef = useRef(null);
  const checkboxRef = useRef(null);
  const label = { inputProps: { 'aria-label': 'Checkbox' } };
  // Todos

  const handleSaveChanges = () => {
    if (todoToEdit) {
      const updatedTodo = { ...todoToEdit, data: updatedData };
      handleTodoUpdate(updatedTodo);
      dispatch(updateTodoData({ id: todoToEdit.id, data: updatedData }));
      handleClose();
    }
  };

  useEffect(() => {
    setUpdatedData(todoToEdit?.data || '');
  }, [todoToEdit]);

  const handleNewTodo = (e) => {
    const inputValue = inputRef.current.value;
    const { checked } = checkboxRef.current;

    if (e.key === 'Enter' && inputValue.trim() !== '') {
      e.preventDefault();
      const ObjTodo = {
        id: uuid(),
        data: inputValue.trim(),
        status: checked ? STATUS_DONE : STATUS_NOT_DONE,
      };

      dispatch(setToDos(ObjTodo));
      inputRef.current.value = '';
      checkboxRef.current.checked = false;
    }
  };

  const handleTodoUpdate = (updatedTodo) => {
    const updatedIndex = todos.findIndex((todo) => todo.id === updatedTodo.id);
    if (updatedIndex !== -1) {
      const updatedTodos = [...todos];
      updatedTodos[updatedIndex] = updatedTodo;
      dispatch(setToDos(updatedTodos));
    }
  };

  // Handle filter selection
  const handleFilter = (selectedFilter) => {
    dispatch(setTodoFilter(selectedFilter));
  };
  const handleUpdateStatus = (id, status) => {
    const objData = {
      id,
      status: status ? STATUS_DONE : STATUS_NOT_DONE,
    };
    dispatch(updateTodoStatus(objData));
  };

  const handleClearStatus = () => {
    dispatch(setTodoClearStatus(STATUS_DONE));
  };

  let filteredTodos;
  // Filter todos based on the selected filter
  if (todoFilter === STATUS_DONE) {
    filteredTodos = todos.filter((todo) => todo.status === STATUS_DONE);
  } else if (todoFilter === STATUS_NOT_DONE) {
    filteredTodos = todos.filter((todo) => todo.status !== STATUS_DONE);
  } else {
    filteredTodos = todos;
  }
  const handleRemove = (selectedRemove) => dispatch(setTodoRemove(selectedRemove));
  const completedCount = filteredTodos ? filteredTodos.reduce((count) => (count += 1), 0) : 0;

  // Themes
  const handleTheme = () => {
    if (theme === 'light') {
      dispatch(setTheme('dark'));
    } else {
      dispatch(setTheme('light'));
    }
  };

  return (
    <div className={classes.todo}>
      <div className={classes.container}>
        <Stack direction="column" spacing={3}>
          <div className={classes.content_title}>
            <div className={classes.text_title}>TODO</div>
            <IconButton className={classes.btn} variant="contained" onClick={handleTheme}>
              {theme === 'light' ? (
                <NightsStayIcon sx={{ fontSize: 36, color: 'white' }} />
              ) : (
                <WbSunnyIcon sx={{ fontSize: 36, color: 'white' }} />
              )}
            </IconButton>
          </div>
          <Paper component="form">
            <Checkbox
              {...label}
              icon={<RadioButtonUncheckedIcon />}
              checkedIcon={<RadioButtonCheckedIcon />}
              onChange={(e) => handleNewTodo(e)}
              inputRef={checkboxRef}
              disabled
            />
            <InputBase
              sx={{ ml: 1, flex: 1, fontSize: '0.8rem' }}
              placeholder=". . .  type your activity"
              inputProps={{ 'aria-label': 'todo list' }}
              onKeyDown={handleNewTodo}
              inputRef={inputRef}
            />
          </Paper>
          <Paper component="form" style={{ maxHeight: 400, overflow: 'auto' }}>
            <Grid container direction="column">
              {filteredTodos &&
                filteredTodos.map((val, i) => (
                  <div className={classes.underline} key={i}>
                    <Grid container alignItems="center" justifyContent="space-between">
                      <Grid item>
                        <div className={classes.inputText}>
                          <Checkbox
                            {...label}
                            icon={<RadioButtonUncheckedIcon />}
                            checkedIcon={<RadioButtonCheckedIcon />}
                            onChange={(e) => handleUpdateStatus(val.id, e.target.checked)}
                          />
                        </div>
                      </Grid>
                      <Grid item xs={6} width="10rem">
                        <div className={classes.main_text}>
                          {val.id === todoToEdit?.id ? updatedData : val.data} {val.status}
                        </div>
                      </Grid>
                      <Grid item>
                        <div className={classes.iconEditClose}>
                          <EditIcon className={classes.edit_icon} onClick={() => handleOpen(val)} />
                          <CloseIcon className={classes.icon_close} onClick={() => handleRemove(val.id)} />
                        </div>
                        {val.id === todoToEdit?.id && (
                          <BasicModal
                            isOpen={openModal}
                            handleToClose={handleClose}
                            todoToEdit={todoToEdit}
                            handleTodoUpdate={handleTodoUpdate}
                            updatedData={updatedData}
                            onSaveChanges={handleSaveChanges}
                          />
                        )}
                      </Grid>
                    </Grid>
                  </div>
                ))}
            </Grid>
          </Paper>
        </Stack>
        <Paper sx={{ p: '10px' }}>
          <Stack direction="row" spacing={3} justifyContent="space-between" alignItems="center">
            <div className={classes.sub_text}>{completedCount} item left</div>
            <Stack direction="row" spacing={2}>
              <div className={classes.sub_text} onClick={() => handleFilter('all')} style={{ cursor: 'pointer' }}>
                All
              </div>
              <div className={classes.sub_text} onClick={() => handleFilter(STATUS_DONE)} style={{ cursor: 'pointer' }}>
                Done
              </div>
              <div
                className={classes.sub_text}
                onClick={() => handleFilter(STATUS_NOT_DONE)}
                style={{ cursor: 'pointer' }}
              >
                Not Done
              </div>
            </Stack>
            <div className={classes.sub_text} onClick={handleClearStatus}>
              Clear Completed
            </div>
          </Stack>
        </Paper>
      </div>
    </div>
  );
};

TodoHome.propTypes = {
  todos: PropTypes.array,
  todoFilter: PropTypes.string,
  theme: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  todos: selectTodoList,
  todoFilter: selectTodoFilter,
  theme: selectTheme,
});

export default connect(mapStateToProps)(TodoHome);
