import { combineReducers } from 'redux';

import appReducer from '@containers/App/reducer';
import languageReducer, { storedKey as storedLangState } from '@containers/Language/reducer';
import todoReducer, { storedKeyTodo as storedKeyTodoState } from '@containers/Todo/reducer';
import themeReducer, { storedKey as storedThemeState } from '@containers/Themes/reducer';

import { mapWithPersistor } from './persistence';

const storedReducers = {
  language: { reducer: languageReducer, whitelist: storedLangState },
  todo: { reducer: todoReducer, whitelist: storedKeyTodoState },
  settheme: { reducer: themeReducer, whitelist: storedThemeState },
};

const temporaryReducers = {
  app: appReducer,
};

const createReducer = () => {
  const coreReducer = combineReducers({
    ...mapWithPersistor(storedReducers),
    ...temporaryReducers,
  });
  const rootReducer = (state, action) => coreReducer(state, action);
  return rootReducer;
};

export default createReducer;
